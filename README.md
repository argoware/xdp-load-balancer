<!--
SPDX-FileCopyrightText: 2023 Luca Bassi

SPDX-License-Identifier: GPL-2.0-or-later
-->

# XDP load balancer

This project was started as part of Luca Bassi's [bachelor's thesis](https://amslaurea.unibo.it/29243/).

To compile XDP programs:

```
clang -O2 -g -Wall -Wno-compare-distinct-pointer-types -target bpf -c <file_name>.c -o <file_name>.o
```

To load XDP programs:

```
sudo xdp-loader unload -a <if_name>; sudo xdp-loader load <if_name> <xdp_program> -m skb
```

To compile userspace programs:

```
gcc <file_name>.c -o <file_name> -lbpf
```
