// SPDX-FileCopyrightText: 2023 Luca Bassi
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <bpf/bpf_endian.h>
#include <bpf/bpf_helpers.h>
#define XXH_INLINE_ALL
#include "xxhash.h"

#define IP_ADDRESS(a, b, c, d) (__be32)(a + (b << 8) + (c << 16) + (d << 24))

#define SERVERS_NUMBER 4

unsigned char server_mac[][ETH_ALEN] = {
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x11 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x12 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x13 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x14 },
};

unsigned int server_weight[] = { 1, 1, 1, 1 };

__be32 virtual_ip = IP_ADDRESS(10, 88, 0, 10);

static __always_inline unsigned int
server_index_ipv4(__be32 saddr, __be16 sport, __be32 daddr, __be16 dport)
{
	XXH64_hash_t max_score = 0;
	unsigned int server_index = 0;
	for (unsigned int i = 0; i < SERVERS_NUMBER; ++i) {
		XXH64_hash_t score = XXH3_64bits_withSeed(
			&i, sizeof(i), saddr ^ sport ^ daddr ^ dport);
		score = score >> 8;
		score *= server_weight[i];
		if (score > max_score) {
			max_score = score;
			server_index = i;
		}
	}
	return server_index;
}

SEC("xdp")
int xdp_hash(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *pos = (void *)(long)ctx->data;
	struct ethhdr *eth = pos;
	if (eth + 1 > data_end) {
		return -1;
	}
	__u16 h_proto = eth->h_proto;
	pos = eth + 1;
	if (h_proto == bpf_htons(ETH_P_IP)) {
		struct iphdr *ip = pos;
		if (ip + 1 > data_end) {
			return -1;
		}
		pos = ip + 1;
		if (ip->protocol == IPPROTO_TCP ||
		    ip->protocol == IPPROTO_UDP) {
			if (ip->daddr == virtual_ip) {
				int server_index;
				if (ip->protocol == IPPROTO_TCP) {
					struct tcphdr *tcp = pos;
					if (tcp + 1 > data_end) {
						return -1;
					}
					server_index = server_index_ipv4(
						ip->saddr, tcp->source,
						ip->daddr, tcp->dest);
				} else {
					struct udphdr *udp = pos;
					if (udp + 1 > data_end) {
						return -1;
					}
					server_index = server_index_ipv4(
						ip->saddr, udp->source,
						ip->daddr, udp->dest);
				}
				__builtin_memcpy(eth->h_source, eth->h_dest,
						 ETH_ALEN);
				__builtin_memcpy(eth->h_dest,
						 server_mac[server_index],
						 ETH_ALEN);
				return XDP_TX;
			}
		}
	}
	return XDP_PASS;
}

char _license[] SEC("license") = "GPL";
