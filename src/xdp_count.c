// SPDX-FileCopyrightText: 2023 Luca Bassi
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <bpf/bpf_endian.h>
#include <bpf/bpf_helpers.h>

struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
	__uint(max_entries, 2);
	__type(key, __u32);
	__type(value, long);
	__uint(pinning, LIBBPF_PIN_BY_NAME);
} count SEC(".maps");

SEC("xdp")
int xdp_count(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *pos = (void *)(long)ctx->data;
	struct ethhdr *eth = pos;
	if (eth + 1 > data_end) {
		return -1;
	}
	__u16 h_proto = eth->h_proto;
	pos = eth + 1;
	__u32 key;
	if (h_proto == bpf_htons(ETH_P_IP)) {
		struct iphdr *ip = pos;
		if (ip + 1 > data_end) {
			return -1;
		}
		__u8 protocol = ip->protocol;
		if (protocol == IPPROTO_ICMP) {
			key = 0;
			long *value = bpf_map_lookup_elem(&count, &key);
			if (value) {
				*value += 1;
			}
		}
	} else if (h_proto == bpf_htons(ETH_P_IPV6)) {
		struct ipv6hdr *ipv6 = pos;
		if (ipv6 + 1 > data_end) {
			return -1;
		}
		__u8 nexthdr = ipv6->nexthdr;
		if (nexthdr == IPPROTO_ICMPV6) {
			key = 1;
			long *value = bpf_map_lookup_elem(&count, &key);
			if (value) {
				*value += 1;
			}
		}
	}
	return XDP_PASS;
}
