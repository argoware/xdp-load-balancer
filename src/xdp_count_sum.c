// SPDX-FileCopyrightText: 2023 Luca Bassi
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/bpf.h>
#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int fd = bpf_obj_get("/sys/fs/bpf/test/count");
	if (fd < 0) {
		printf("Error bpf_obj_get\n");
		return fd;
	}
	int nr_cpus = libbpf_num_possible_cpus();
	long count_sum[] = { 0, 0 };
	for (__u32 key = 0; key <= 1; ++key) {
		long values[nr_cpus];
		if ((bpf_map_lookup_elem(fd, &key, values)) != 0) {
			printf("Error bpf_map_lookup_elem\n");
			return -1;
		}
		for (int i = 0; i < nr_cpus; ++i) {
			count_sum[key] += values[i];
		}
	}
	printf("IPv4: %d\nIPv6: %d\n", count_sum[0], count_sum[1]);
	return 0;
}
