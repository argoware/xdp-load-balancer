// SPDX-FileCopyrightText: 2023 Luca Bassi
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <bpf/bpf_endian.h>
#include <bpf/bpf_helpers.h>
//#include "MurmurHash3.h"
#define XXH_INLINE_ALL
#include "xxhash.h"

#define IP_ADDRESS(a, b, c, d) (__be32)(a + (b << 8) + (c << 16) + (d << 24))

#define SERVERS_NUMBER 4

unsigned char server_mac[][ETH_ALEN] = {
	{ 0xfa, 0x16, 0x3e, 0x76, 0x51, 0xf9 },
	{ 0xfa, 0x16, 0x3e, 0x9c, 0x38, 0x17 },
	{ 0xfa, 0x16, 0x3e, 0x01, 0x7d, 0xd9 },
	{ 0xfa, 0x16, 0x3e, 0xc4, 0x77, 0xcb },
};

unsigned int server_weight[] = { 1, 1, 1, 1 };

__be32 virtual_ip = IP_ADDRESS(192, 168, 23, 250);
__u8 virtual_ipv6[16] = { 0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			  0xf8, 0x16, 0x3e, 0xff, 0xfe, 0x76, 0x51, 0xf9 };

static __always_inline unsigned int
server_index_ipv4(__be32 saddr, __be16 sport, __be32 daddr, __be16 dport)
{
	XXH64_hash_t max_score = 0;
	//unsigned int max_score = 0;
	unsigned int server_index = 0;
	for (unsigned int i = 0; i < SERVERS_NUMBER; ++i) {
		//unsigned int score;
		//MurmurHash3_x86_32(&i, sizeof(i), saddr ^ sport ^ daddr ^ dport,
		//		   &score);
		XXH64_hash_t score = XXH3_64bits_withSeed(
			&i, sizeof(i), saddr ^ sport ^ daddr ^ dport);
		score = score >> 8;
		score *= server_weight[i];
		if (score > max_score) {
			max_score = score;
			server_index = i;
		}
	}
	return server_index;
}

static __always_inline unsigned int server_index_ipv6(struct in6_addr *saddr,
						      __be16 sport,
						      struct in6_addr *daddr,
						      __be16 dport)
{
	//XXH64_hash_t max_score = 0;
	unsigned int max_score = 0;
	unsigned int server_index = 0;
	for (unsigned int i = 0; i < SERVERS_NUMBER; ++i) {
		unsigned int score = 0;
		//XXH64_hash_t score = XXH3_64bits_withSeed(
		//	&i, sizeof(i),
		//	(*saddr).s6_addr32[0] ^ (*saddr).s6_addr32[1] ^
		//		(*saddr).s6_addr32[2] ^ (*saddr).s6_addr32[3] ^
		//		sport ^ (*daddr).s6_addr32[0] ^
		//		(*daddr).s6_addr32[1] ^ (*daddr).s6_addr32[2] ^
		//		(*daddr).s6_addr32[3] ^ dport);
		score = score >> 8;
		score *= server_weight[i];
		if (score > max_score) {
			max_score = score;
			server_index = i;
		}
	}
	return server_index;
}

SEC("xdp")
int xdp_hash(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *pos = (void *)(long)ctx->data;
	struct ethhdr *eth = pos;
	if (eth + 1 > data_end) {
		return -1;
	}
	__u16 h_proto = eth->h_proto;
	pos = eth + 1;
	if (h_proto == bpf_htons(ETH_P_IP)) {
		struct iphdr *ip = pos;
		if (ip + 1 > data_end) {
			return -1;
		}
		pos = ip + 1;
		if (ip->protocol == IPPROTO_TCP ||
		    ip->protocol == IPPROTO_UDP) {
			if (ip->daddr == virtual_ip) {
				int server_index = 0;
				if (ip->protocol == IPPROTO_TCP) {
					struct tcphdr *tcp = pos;
					if (tcp + 1 > data_end) {
						return -1;
					}
					if (bpf_htons(tcp->dest) == 22 ||
					    bpf_htons(tcp->source) == 53) {
						return XDP_PASS;
					}
					struct bpf_sock_tuple tuple;
					tuple.ipv4.saddr = ip->saddr;
					tuple.ipv4.daddr = ip->daddr;
					tuple.ipv4.sport = tcp->source;
					tuple.ipv4.dport = tcp->dest;
					struct bpf_sock *socket =
						bpf_sk_lookup_tcp(
							ctx, &tuple,
							sizeof(tuple.ipv4), -1,
							0);
					if (socket) {
						bpf_sk_release(socket);
						return XDP_PASS;
					}
					server_index = server_index_ipv4(
						ip->saddr, tcp->source,
						ip->daddr, tcp->dest);
				} else {
					struct udphdr *udp = pos;
					if (udp + 1 > data_end) {
						return -1;
					}
					if (bpf_htons(udp->dest) == 22 ||
					    bpf_htons(udp->source) == 53 ||
					    (bpf_htons(udp->source) == 67 &&
					     bpf_htons(udp->dest) == 68)) {
						return XDP_PASS;
					}
					server_index = server_index_ipv4(
						ip->saddr, udp->source,
						ip->daddr, udp->dest);
				}
				__builtin_memcpy(eth->h_source, eth->h_dest,
						 ETH_ALEN);
				__builtin_memcpy(eth->h_dest,
						 server_mac[server_index],
						 ETH_ALEN);
				return XDP_TX;
			}
		}
	} else if (h_proto == bpf_htons(ETH_P_IPV6)) {
		struct ipv6hdr *ipv6 = pos;
		if (ipv6 + 1 > data_end) {
			return -1;
		}
		pos = ipv6 + 1;
		if (ipv6->nexthdr == IPPROTO_TCP ||
		    ipv6->nexthdr == IPPROTO_UDP) {
			if (__builtin_memcmp(ipv6->daddr.s6_addr, virtual_ipv6,
					     sizeof(ipv6->daddr))) {
				int server_index;
				if (ipv6->nexthdr == IPPROTO_TCP) {
					struct tcphdr *tcp = pos;
					if (tcp + 1 > data_end) {
						return -1;
					}
					if (bpf_htons(tcp->dest) == 22 ||
					    bpf_htons(tcp->source) == 53) {
						return XDP_PASS;
					}
					server_index = server_index_ipv6(
						&(ipv6->saddr), tcp->source,
						&(ipv6->daddr), tcp->dest);
				} else {
					struct udphdr *udp = pos;
					if (udp + 1 > data_end) {
						return -1;
					}
					if (bpf_htons(udp->dest) == 22 ||
					    bpf_htons(udp->source) == 53 ||
					    (bpf_htons(udp->source) == 67 &&
					     bpf_htons(udp->dest) == 68)) {
						return XDP_PASS;
					}
					server_index = server_index_ipv6(
						&(ipv6->saddr), udp->source,
						&(ipv6->daddr), udp->dest);
				}
				__builtin_memcpy(eth->h_source, eth->h_dest,
						 ETH_ALEN);
				__builtin_memcpy(eth->h_dest,
						 server_mac[server_index],
						 ETH_ALEN);
				return XDP_TX;
			}
		}
	}
	return XDP_PASS;
}

char _license[] SEC("license") = "GPL";
