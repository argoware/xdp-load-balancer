// SPDX-FileCopyrightText: 2023 Luca Bassi
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int weights = bpf_obj_get("/sys/fs/bpf/test/weights");
	if (weights < 0) {
		printf("Error bpf_obj_get\n");
		return weights;
	}
	for (__u32 server_index = 0; server_index < 4; ++server_index) {
		printf("Weight server%d: \n", server_index + 1);
		long weight;
		scanf("%ld", &weight);
		if (bpf_map_update_elem(weights, &server_index, &weight,
					BPF_EXIST) != 0) {
			printf("Error bpf_map_update_elem\n");
			return -1;
		}
	}
	return 0;
}
