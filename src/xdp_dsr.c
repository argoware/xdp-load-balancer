// SPDX-FileCopyrightText: 2023 Luca Bassi
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <bpf/bpf_endian.h>
#include <bpf/bpf_helpers.h>

#define IP_ADDRESS(a, b, c, d) (__be32)(a + (b << 8) + (c << 16) + (d << 24))

unsigned char load_balancer_mac[ETH_ALEN] = {
	0x02, 0x42, 0x00, 0x00, 0x00, 0x10
};

unsigned char server_mac[][ETH_ALEN] = {
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x11 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x12 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x13 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x14 },
};

__be32 virtual_ip = IP_ADDRESS(10, 88, 0, 10);

SEC("xdp")
int xdp_dsr(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *pos = (void *)(long)ctx->data;
	struct ethhdr *eth = pos;
	if (eth + 1 > data_end) {
		return -1;
	}
	__u16 h_proto = eth->h_proto;
	pos = eth + 1;
	if (h_proto == bpf_htons(ETH_P_IP)) {
		struct iphdr *ip = pos;
		if (ip + 1 > data_end) {
			return -1;
		}
		if (ip->protocol == IPPROTO_TCP ||
		    ip->protocol == IPPROTO_UDP) {
			if (ip->daddr == virtual_ip) {
				__builtin_memcpy(eth->h_source,
						 load_balancer_mac,
						 sizeof(load_balancer_mac));
				__builtin_memcpy(
					eth->h_dest,
					server_mac[(bpf_ktime_get_ns() /
						    2000000000) %
						   4],
					sizeof(server_mac[0]));
				return XDP_TX;
			}
		}
	}
	return XDP_PASS;
}

char _license[] SEC("license") = "GPL";
