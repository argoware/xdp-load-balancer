// SPDX-FileCopyrightText: 2023 Luca Bassi
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <bpf/bpf_endian.h>
#include <bpf/bpf_helpers.h>

#define IP_ADDRESS(a, b, c, d) (__be32)(a + (b << 8) + (c << 16) + (d << 24))

static __always_inline __u16 csum_fold_helper(__u64 csum)
{
	int i;
#pragma unroll
	for (i = 0; i < 4; i++) {
		if (csum >> 16) {
			csum = (csum & 0xffff) + (csum >> 16);
		}
	}
	return ~csum;
}

static __always_inline __u16 iph_csum(struct iphdr *iph)
{
	iph->check = 0;
	unsigned long long csum = bpf_csum_diff(0, 0, (unsigned int *)iph,
						sizeof(struct iphdr), 0);
	return csum_fold_helper(csum);
}

unsigned char client_mac[ETH_ALEN] = { 0x02, 0x42, 0x00, 0x00, 0x00, 0x09 };

unsigned char load_balancer_mac[ETH_ALEN] = {
	0x02, 0x42, 0x00, 0x00, 0x00, 0x10
};

unsigned char server_mac[][ETH_ALEN] = {
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x11 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x12 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x13 },
	{ 0x02, 0x42, 0x00, 0x00, 0x00, 0x14 },
};

__be32 client_ip = IP_ADDRESS(10, 88, 0, 9);

__be32 load_balancer_ip = IP_ADDRESS(10, 88, 0, 10);

__be32 server_ip[] = {
	IP_ADDRESS(10, 88, 0, 11),
	IP_ADDRESS(10, 88, 0, 12),
	IP_ADDRESS(10, 88, 0, 13),
	IP_ADDRESS(10, 88, 0, 14),
};

unsigned int backend_server_index = 0;

SEC("xdp")
int xdp_redirect(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *pos = (void *)(long)ctx->data;
	struct ethhdr *eth = pos;
	if (eth + 1 > data_end) {
		return -1;
	}
	__u16 h_proto = eth->h_proto;
	pos = eth + 1;
	if (h_proto == bpf_htons(ETH_P_IP)) {
		struct iphdr *ip = pos;
		if (ip + 1 > data_end) {
			return -1;
		}
		if (ip->protocol == IPPROTO_TCP ||
		    ip->protocol == IPPROTO_UDP) {
			if (ip->saddr == client_ip) {
				bpf_printk("Sending request to server%d",
					   backend_server_index + 1);
				if (backend_server_index < 4) {
					__builtin_memcpy(
						eth->h_dest,
						server_mac[backend_server_index],
						sizeof(server_mac
							       [backend_server_index]));
					ip->daddr =
						server_ip[backend_server_index];
				}
				backend_server_index =
					(backend_server_index + 1) %
					(sizeof(server_ip) /
					 sizeof(server_ip[0]));
			} else if (ip->saddr == server_ip[0] ||
				   ip->saddr == server_ip[1] ||
				   ip->saddr == server_ip[2] ||
				   ip->saddr == server_ip[3]) {
				bpf_printk("Response from server%d",
					   (ip->saddr >> 24) % 10);
				__builtin_memcpy(eth->h_dest, client_mac,
						 sizeof(client_mac));
				ip->daddr = client_ip;
			}
			__builtin_memcpy(eth->h_source, load_balancer_mac,
					 sizeof(load_balancer_mac));
			ip->saddr = load_balancer_ip;
			ip->check = iph_csum(ip);
			return XDP_TX;
		}
	}
	return XDP_PASS;
}

char _license[] SEC("license") = "GPL";
